#include "login.h"
#include "ui_login.h"

#include <QSqlDatabase>
#include <QtSql/QSqlError>
#include <QtSql/QSqlQuery>
#include <QMessageBox>
#include <QDebug>

Login::Login(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Login)
{
    ui->setupUi(this);

    QSqlDatabase db = QSqlDatabase::addDatabase("QPSQL");
    db.setHostName("localhost");
    db.setDatabaseName("registroDev");
    db.setPort(5432);
    db.setUserName("postgres");
    db.setPassword("Lb1045723");

    if(!db.open()){
        QMessageBox::warning(this, "Error", db.lastError().text());
    }else{
        QMessageBox::information(this, "Genial", "Conexion con DB exitosa");
        show();
    }
}

Login::~Login()
{
    delete ui;
}

void Login::on_pushButton_clicked()
{
    QString sid = ui->lineEdit_ID->text();
    QString sclave = ui->lineEdit_CLAVE->text();

    if(sid == NULL && sclave == NULL)
    {
        QMessageBox::warning(this, "Error", "Los campos estan vacios");
    }
    else if(sid == NULL)
    {
        QMessageBox::warning(this, "Error", "El campo id esta vacio.");
    }
    else if (sclave == NULL)
    {
        QMessageBox::warning(this, "Error", "El campo clave esta vacio.");
    }
    else
    {
        QSqlQuery query;
        query.exec("SELECT username, password FROM registros WHERE username='" + sid + "' AND password='" + sclave + "';");

        if(query.next() == true)
        {
            QMessageBox::warning(this, "Enable", "Hola amigo!");
        }
        else
        {
            QMessageBox::warning(this, "Error", "Esta cuenta no existe");
        }

    }
}
